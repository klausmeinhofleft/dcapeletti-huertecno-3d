# HuerTecno-3D


![Semantic description of image](https://git.rlab.be/dcapeletti/huertecno-3d/raw/master/Recursos/Logos/Logo%20HuerTecno-3D.png "HuerTecno en 3D")



Este repositorio es un trabajo en progreso. El mismo alberga el diseño 3D de equipamientos, herramientas físicas que se realizan, se utilizan o se 
producen en el  [proyecto HuerTecno](https://0xacab.org/hueretechno)

Por tanto, queremos que este trabajo sea útil para investigar cómo funcionan "las cosas". También te puede servir por si quieres fabricar algo tu mismo y entender 
qué necesitas para construir eso que deseas.

Nuestro objetivo final es que cualquier componente físico pueda ser leído y replicable fácilmente por cualquier humano. Eso significa que lo puedas ver y así entender
como está hecho, proponer cambios o incluso mejorarlo tu mismo. Todas las contribuciones, dudas o quejas son bienvenidas.

## **ESPECIFICACIÓN DEL SOFTWARE Y HERRAMIENTAS DE DESARROLLO**

Para el diseño de piezas y ensambles usamos FreeCAD en su versión 0.18. La web aquí para descargarlo https://freecadweb.org/

Además utilizamos los siguientes bancos de trabajos (workbench) dentro de FreeCAD : A2Plus entre otros. Si deseas contribuir con este proyecto, es necesario que 
instales los bancos mencionados anteriormente desde el Addon Manager dentro de FreeCAD.

Si quieres aprender a usar FreeCAD, primero leer la wiki y mira vídeos en internet. 
[Aquí hay mas de 100 ejercicios CAD que puedes hacerlo tu mismo](https://forum.freecadweb.org/viewtopic.php?f=3&t=31006&start=30).
Luego puedes visitar el foro y consultar en el mismo, es muy activo y amigable. Aprender a usar bien FreeCAD te puede llevar de 4 a 6 meses aproximadamente.

También usamos Inkscape para calcar dibujos y posiblemnte usemos Blender mas adelante.


## **LICENCIA DE LOS ARCHIVOS DEL PROYECTO**

Todas los archivos de diseño FCSTD (FreeCAD Standard file format) de este repositorio están licenciadas bajo CC-BY 3.0 http://creativecommons.org/licenses/by/3.0/ 

Cada pieza o conjunto está protegido por derechos de autor y debe ser atribuida a su(s) autor(es) respectivo(s). Vea los detalles del commit para encontrar a los autores
de cada parte.

Si está subiendo piezas o conjuntos a este repositorio, por favor asegúrese de que usted es el autor del modelo, o de lo contrario, tiene derecho a compartirlo 
aquí bajo la licencia CC-BY 3.0 y asegúrese de que el autor es mencionado en el mensaje de confirmación.


## **ORGANIZACIÓN DEL REPOSITORIO**

**1. Archivos y directorios de proyectos.**

Cada proyecto tiene su carpeta general que llevará el nombre del mismo. 

Dentro de ella, se tiene un directorio de **Partes** y directorios de diferentes **Subconjuntos** si es necesario y un archivo de **ensamble final**.
A su vez, cada proyecto puede tener partes comunes con otros, por lo que dicha pieza, debería moverse al directorio de Librerías.
Ejemplo de organización del repositorio para el proyecto Hidroponia:
    

    Librerías
    |--->Bulonería
    |--->Perfilería
    Hidroponia
    |--->Partes
        |--->Parte A
        |--->Parte B
    |--->Subconjuntos
        |--->Subconjunto A
        |--->Subconjunto B
    |--->Ensamble final.fcstd
    |--->Recursos
        |--->Fotos reales
            |--->Foto1.jpg
        |--->Renders 3D
            |--->Vista superior.jpg
            |--->Vista axonométrica.jpg
 
    
El directorio de librerías podría ser útil para contribuir con https://github.com/FreeCAD/FreeCAD-library/


**2. Directorio de piezas (o partes) y archivos de diseño.**

Cada pieza intenta ser lo mas paramétrica posible. Esto permite tener mayor libertad a la hora de ajustar los parámetros de construcción. Por tanto, si vez que una
    pieza que es "inadaptable", puedes proponer los parámetros que quieras para que su construcción sea mas flexible para el usuario.
    Cada pieza, debe tener una hoja de cálculo llamada "Variables". Esta sirve para que el usuario pueda ajustar los parámetros de construcción sin tocar el diseño.
    
Cada archivo Parte además debe tener una hoja de cálculo #PARTINFO# que indica la información de dicha pieza. Únicamente se cargará la descripción de la misma,
    el resto se puede llenar a discreción cuando se genera la lista de materiales.
    Las piezas, también podría contener o no, planos en 2D para imprimir el dibujo o para solicitar cotización a un proveedor.
    Algo deseable es que cada pieza sea exportada al formato STL ya que esto garantiza que la misma se pueda visualizar en el navegador web.

**3. Directorios de Subconjuntos y archivos de ensamble.**

Son archivo que muestran como se unen las partes para formar un conjunto. Se usa A2Plus en FreeCAD para realizarlos.

Cada archivo de conjunto debe tener una hoja de cálculo llamada #PARTLIST# que contiene la lista de partes del ensamble. Se genera con A2Plus.

Si necesitas crear varios subconjuntos para diminuir la complejidad o porque así se diseña el mecanismo, crea un directorio para ese subconjunto con un nombre
que lo identifique. En el ejemplo anterior hay dos directorios para Subconjunto A y B.

Es deseable que los archivos de ensamble, al igual que el archivo de piezas, tengan su correspondiente plano en 2D y el archivo STL para poder mirarlo en un navegador web.

**4. Listado de materiales (Bill of Materials – BOM).**

Generalmente es una hoja de cálculo que contiene la información de todas las piezas, cantidad, descripción, etc. de todo el conjunto o de una máquina completa.
Esta lista de materiales es generada automáticamente por FreeCAD usando el banco de trabajo A2Plus. 

La principal finalidad de una lista de materiales es para determinar que materiales debes comprar, cantidades y especificaciones de los mismos. 
Una vez que tienes estos datos, puedes contactar con diversos proveedores y obtener cotización para armar tu máquina.

**5. Otros directorios**

Puedes crear otros directorios para el proyecto si es necesario, por ejemplo Recursos. Este puede servir para guardar imágenes o vídeos de algo real.
También si lo deseas, puede crear un directorio de Renders que pueden ser útiles para mostrar el diseño "realista" de la máquina o pieza.



## **PREGUNTAS FRECUENTES**

**Quiero imprimir el logo de HuerTecno en 3D. ¿Cómo hago y qué necesito saber?**

Ve al directorio huertecno-3d/Recursos/Logos y busca cualquier archivo .stl que mas te guste. Los archivos .stl son archivos de mallas que se envían
a una impresora 3D. Hay una lista de proveedores impresión 3D en **huertecno-3d/Recursos/Servicios impresión 3D** que podrías usar para solicitar
cotización de tu impresión. 

Antes que nada, asegúrate de las dimensiones de la impresión son las correctas. Puedes escalar el modelo al tamaño que tu quieras. 
Y por otra parte, el material, que pueden ser PLA y ABS. Y por último el color que quieras. Todo esto lo negocias con tu proveedor de impresión 3D.

**No soy diseñador 3D, pero quiero modificar el tamaño de una pieza o cambiar algo en ella ¿Cómo hago?**

Dentro del archivo de la pieza deberías encontrar una hoja de cálculo llamada "Variables". Modifica las variables que se listan y FreeCAD construirá la pieza por tí
según los valores que hayas puesto en los parámetros.
Si quieres que algo mas sea parametrizable, puedes enviar un issue para lo que deseas.

**Si modifico una pieza que es usada en varios conjuntos. ¿Qué sucede con ellos?**

FreeCAD recalcula el diseño del conjunto de acuerdo a las restricciones de las piezas. Por tanto cada conjunto que la usa, va a leer los últimos cambios realizados
a la/las piezas y ajustará automáticamente todo el diseño. Si algunas restricciones no se cumplen, freecad te avisará que algo anda mal.

**Quiero modificar el detalle de una o varias piezas en el lista de materiales.**

Cada pieza tiene una hoja de cálculo llamada #PARTINFO# que puedes usar para modificar lo que necesites.

**¿Puedo usar FreeCAD en GNU Linux, en windows o en MAC?**

Sí, puede usarlo en cualquier sistema operativo. Pero pedimos que cumplas con los path especificados en 
**[ORGANIZACIÓN DEL REPOSITORIO](https://git.rlab.be/dcapeletti/huertecno-3d/tree/master#organizaci%C3%B3n-del-repositorio)** para que todos los conjuntos
funcionen en todos lados.





